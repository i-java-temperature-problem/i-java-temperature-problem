package i.java.temperature.problem.servei;

import java.util.List;

import i.java.temperature.problem.domini.Registre;

public interface ServeiRegistresInterface {

    public List<Registre> getRegistres();

    public List<Registre> getRegistreAula(int aula);

    public void addRegistre(Registre r);

    public void updateRegistre(Long id, Registre registre) throws Exception;

    public void deleteRegistre(Long id);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i.java.temperature.problem.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;

/**
 *
 * @author Kirasumairu
 */
@Configuration
@EnableSwagger2
public class SwaggerController extends WebMvcConfigurationSupport {

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                //Cambiar por nuestro paquete donde está el controlador con los métodos implementados
                .select().apis(RequestHandlerSelectors.basePackage("i.java.temperature.problem.controller"))
                //Cambiar por nuestro Requestmapping del controlador
                .paths(regex("/api.*"))
                .build()
                //Para añadir información de contacto
                .apiInfo(metaData());
    }

    //Añadir información de contacto
    private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title("Spring Boot REST API")
                .description("\"Spring Boot REST API for projects\"")
                .version("1.0.0")
                .contact(new Contact("Grup Projecte M6", "https://gitlab.com/i-java-temperature-problem/i-java-temperature-problem", ""))
                .build();
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}

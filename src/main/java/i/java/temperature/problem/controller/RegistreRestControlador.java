package i.java.temperature.problem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import i.java.temperature.problem.domini.Registre;
import i.java.temperature.problem.servei.ServeiRegistresInterface;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

//Controller -> No funciona
@RestController
@RequestMapping("/api")
//Cambiar la descripción en el swagger
@Api(value = "controller", description = "Operacions del controlador de registres")
public class RegistreRestControlador {

    //Las interfaces tienen que estar definidas con AutoWired para la correcta inyección
    @Autowired
    private ServeiRegistresInterface serveiRegistres;

    //Muestra la descripción de la función en el swagger
    @ApiOperation(value = "Obtenir tots els registres")
    @RequestMapping(value = "/registres", method = RequestMethod.GET)
    public List<Registre> obtenirRegistres() {
        return serveiRegistres.getRegistres();
    }

    @ApiOperation(value = "Obtenir tots els registres d'una aula")
    @RequestMapping(value = "/registresAula/{aula}", method = RequestMethod.GET)
    public List<Registre> obtenirRegistres(@PathVariable("aula") int aula) {
        return serveiRegistres.getRegistreAula(aula);
    }

    @ApiOperation(value = "Afegir un registre")
    @RequestMapping(value = "/afegirRegistre", method = RequestMethod.POST)
    public ResponseEntity<Boolean> afegirRegistre(@RequestBody Registre registre) {
        serveiRegistres.addRegistre(registre);
        return new ResponseEntity<>(true, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Modificar un registre")
    @RequestMapping(value = "/modificarRegistre/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> modificarPersona(@RequestBody Registre registre, @PathVariable("id") Long id) {
        try {
            serveiRegistres.updateRegistre(id, registre);
            return new ResponseEntity<>(true, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Eliminar un registre")
    @RequestMapping(value = "/eliminarRegistre/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> eliminarersona(@PathVariable("id") Long id) {
        serveiRegistres.deleteRegistre(id);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}

# Projecte de M6

Projecte Rest API en JAVA M6 DAM

![Swagger API](http://oi68.tinypic.com/2dihcty.jpg)

## Instal·lació

Es necessita un IDE (NetBeans/Eclipse/IntelliJIdea) per a obrirlo i executar el projecte.

També es necessita un servidor MySQL amb la següent estructura:

~~~~sql
id integer primary key auto_increment,
temperatura float not null,
humedad float not null,
aula int not null,
data DATETIME not null
~~~~

Adicionalment es necessita un dispositiu android per a provar l'aplicació client.

## Configuració

### Arxiu application.properties
```
Modificar el spring.datasource.url per la teva url on està la BD
```

### Arxiu i.java.temperature.problem.domini.Registre
```
Canviar el nom de @Entity pel nom de la teva taula
```

### Arxius de l'aplicació d'Android
```
Modificar la constant IPAPI per la url on està iniciada la Rest API
```

## Swagger
Per a poder provar la funcionalitat de Swagger s'ha d'anar a la ruta [SwaggerTest](http://yourIP:8080/swagger-ui.html)

## Participants
Marc Amorós

Mario Sangorrín

Robin Morató